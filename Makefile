SRCS = main.c swish.c lineeditor.c vector.c dict.c strdict.c os.c
SRCDIR = src
INCDIR = inc
OBJDIR ?= obj
OBJS = $(patsubst %.c,$(OBJDIR)/%.o,$(SRCS))
DEPS = $(patsubst %.c,$(OBJDIR)/%.d,$(SRCS))

EXE = $(OBJDIR)/swish

CFLAGS = -MMD -g -Wall -Wextra -Wshadow -Werror 
INCLUDES = -I$(INCDIR)

all: $(EXE)

$(EXE): $(OBJS)
	$(CC) $(CFLAGS) -o $(EXE) $(OBJS)

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $(INCLUDES) -o $@ -c $<

clean:
	rm -f $(EXE) $(OBJS) $(DEPS)

.PHONY: all clean

-include $(DEPS)
