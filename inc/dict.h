//
// A dictionary is a mapping from strings to something else.
// For instance a dictionary of strings maps from string keys to string 
// values.
//
// The dictionary is implemented here as a sorted vector of key/value pairs.
//

#ifndef SWISH_DICT_H
#define SWISH_DICT_H

#include "common.h"
#include "vector.h"


typedef struct 
{
    char *key;
    void *value;
} SwishDictKv;

typedef void (*SwishFreeValueFunc)(void *);

typedef struct 
{
    SwishVector v;
    SwishFreeValueFunc free_value;
} SwishDict;

// Prototypes.
WARN_UNUSED SwishResult SwishDictInit(SwishDict *d, SwishFreeValueFunc free_value);
            void        SwishDictDeinit(SwishDict *d);
WARN_UNUSED SwishResult SwishDictAdd(SwishDict *d, const char *key, void *value);
            bool        SwishDictDelete(SwishDict *d, const char *key);
WARN_UNUSED void *      SwishDictFind(SwishDict *d, const char *key);

#endif // SWISH_DICT_H
