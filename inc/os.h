#ifndef SWISH_OS_H
#define SWISH_OS_H

#include <unistd.h>

// Prototypes.
void *SwishOsMalloc(size_t size);
void *SwishOsRealloc(void *mem, size_t size);
void SwishOsFree(void *mem);
char SwishOsGetc(int fd);
void SwishOsPutc(int fd, char ch);
void SwishOsPuts(int fd, const char *buf);
void SwishOsWrite(int fd, const char *buf, size_t len);

#endif // SWISH_OS_H
