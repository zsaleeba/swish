#ifndef SWISH_LINEEDITOR_H
#define SWISH_LINEEDITOR_H

#include <unistd.h>
#include <stdbool.h>

#include "common.h"


// Key codes in additional to the usual ASCII ones.
typedef enum
{
    SWISH_KEYCODE_UP_ARROW = 256,
    SWISH_KEYCODE_DOWN_ARROW,
    SWISH_KEYCODE_LEFT_ARROW,
    SWISH_KEYCODE_RIGHT_ARROW,
    SWISH_KEYCODE_HOME,
    SWISH_KEYCODE_END,
    SWISH_KEYCODE_INSERT,
    SWISH_KEYCODE_DELETE,
    SWISH_KEYCODE_PAGE_UP,
    SWISH_KEYCODE_PAGE_DOWN
} SwishKeyCode;


// Line editor data type.
typedef struct 
{
    // fds to edit with.
    int          inFd;
    int          outFd;
    unsigned int historyMaxLines;

    // Line buffer.
    char *      line;
    unsigned int space;
    unsigned int used;

    // History buffer.
    char **      history;
    unsigned int historyLines;

    // Edit state.
    unsigned int cursorX;
    unsigned int historyCurrentLine;

} SwishLineEditor;

// Prototypes.
WARN_UNUSED SwishResult SwishLineEditorInit(SwishLineEditor *le, int inFd, int outFd, int historyMaxLines);
            void        SwishLineEditorDeinit(SwishLineEditor *le);
WARN_UNUSED SwishResult SwishLineEditorGetLine(SwishLineEditor *le, char **line);
WARN_UNUSED SwishResult SwishLineEditorGetKey(SwishLineEditor *le, int *key);

#endif // SWISH_LINEEDITOR_H
