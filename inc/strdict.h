//
// A StrDict is a dictionary of strings. It's useful for environment and
// symbol tables.
//
// The StrDict is implemented here using Dict, which is in turn implemented
// as a sorted vector of key/value pairs.
//

#ifndef SWISH_STRDICT_H
#define SWISH_STRDICT_H

#include "common.h"
#include "vector.h"
#include "dict.h"


typedef SwishDict SwishStrDict;

// Prototypes.
WARN_UNUSED SwishResult SwishStrDictInit(SwishStrDict *sd);
            void        SwishStrDictDeinit(SwishStrDict *sd);
WARN_UNUSED SwishResult SwishStrDictAdd(SwishStrDict *sd, const char *key, const char *value);
            bool        SwishStrDictDelete(SwishStrDict *sd, const char *key);
WARN_UNUSED const char *SwishStrDictFind(SwishStrDict *sd, const char *key);

#endif // SWISH_STRDICT_H
