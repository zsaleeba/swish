#ifndef SWISHRESULT_H
#define SWISHRESULT_H


// Attribute to warn about unused return values.
#define WARN_UNUSED __attribute__((warn_unused_result))


// Result codes.
typedef enum 
{
    SWISH_OK = 0,
    SWISH_ERROR_OUT_OF_MEMORY,
    SWISH_ERROR_IO
} SwishResult;

#endif // SWISHRESULT_H
