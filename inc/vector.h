#ifndef SWISH_VECTOR_H
#define SWISH_VECTOR_H

#include <unistd.h>

#include "common.h"

// Generic-ish way of accessing items in the vector.
#define VEC(v,i,t) (((t*)((v)->vec))[(i)])

// Vector data type.
typedef struct 
{
    void * vec;         // The variable-size vector.
    size_t item_size;   // The size of each item in the vector.
    size_t items;       // The number of items in the vector.
    size_t space;       // The maximum number items we currently have space for in the vector.

} SwishVector;

// Prototypes.
WARN_UNUSED SwishResult SwishVectorInit(SwishVector *v, size_t item_size, size_t start_space);
            void        SwishVectorDeinit(SwishVector *v);
WARN_UNUSED SwishResult SwishVectorAdd(SwishVector *le, unsigned int index, void *new_item_ptr);
            void        SwishVectorDelete(SwishVector *le, unsigned int index);

#endif // SWISH_VECTOR_H
