#ifndef SWISH_H
#define SWISH_H

#include "lineeditor.h"
#include "common.h"


// Swish shell data type.
typedef struct 
{
    // fds to communicate on.
    int    inFd;
    int    outFd;
    int    historyMaxLines;

    // Line editor.
    SwishLineEditor lineEditor;
} SwishShell;


// Command definiton.
typedef struct
{
    const char *cmd;
} SwishCommandDef;


// Prototypes.
WARN_UNUSED SwishResult SwishShellInit(SwishShell *ss, int inFd, int outFd, int historyMaxLines);
            void        SwishShellDeinit(SwishShell *ss);
WARN_UNUSED SwishResult SwishShellCommandLine(SwishShell *ss);
WARN_UNUSED SwishResult SwishShellAddCommand(SwishShell *ss, SwishCommandDef *cmd);
WARN_UNUSED const char *SwishErrorStr(SwishResult r);

#endif // SWISH_H
