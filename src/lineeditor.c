#include <string.h>
#include <stdlib.h>

#include "common.h"
#include "lineeditor.h"
#include "swish.h"
#include "os.h"


// Key codes.
#define ESCAPE_KEY    0x1b
#define BACKSPACE_KEY 0x7f
#define ENTER_KEY     '\r'

// Line buffers start this long but may grow.
#define START_LINE_BUFFER_LEN 80


//
// Initialises the line editor. This dynamically allocates storage so 
// you must call SwishLineEditorDeinit() when you're done.
//

WARN_UNUSED SwishResult SwishLineEditorInit(SwishLineEditor *le, int inFd, int outFd, int historyMaxLines)
{
    memset(le, 0, sizeof(*le));

    // fds to edit with.
    le->inFd  = inFd;
    le->outFd = outFd;
    le->historyMaxLines = historyMaxLines;

    // History buffer.
    if (historyMaxLines > 0)
    {
        le->history = SwishOsMalloc(historyMaxLines * sizeof(char *));
        if (le->history == NULL)
            return SWISH_ERROR_OUT_OF_MEMORY;
    }

    // Line buffer.
    le->space = START_LINE_BUFFER_LEN;
    le->used  = 0;
    le->line  = SwishOsMalloc(le->space);
    memset(le->line, 0, le->space);

    le->historyLines = 0;
    le->cursorX = 0;
    le->historyCurrentLine = 0;

    return SWISH_OK;
}


//
// Frees all resources associated with the line editor.
//

void SwishLineEditorDeinit(SwishLineEditor *le)
{
    // Free the history.
    if (le->history != NULL)
    {
        for (unsigned int i = 0; i < le->historyLines; i++)
        {
            free(le->history[i]);
        }

        free(le->history);
        le->history = NULL;
    }

    // Free the current line.
    if (le->line != NULL)
    {
        free(le->line);
        le->line = NULL;
    }
}


//
// Draw a single character repeatedly.
//

static void DrawRepeated(SwishLineEditor *le, char ch, int reps)
{
    for (int i = 0; i < reps; i++)
    {
        SwishOsPutc(le->outFd, ch);
    }
}


//
// Move the screen cursor back.
//

static void DrawMoveBack(SwishLineEditor *le, int dist)
{
    DrawRepeated(le, '\b', dist);
}


//
// Move the screen cursor forward from a given location to
// another location.
//

static void DrawMoveForward(SwishLineEditor *le, int from, int to)
{
    SwishOsWrite(le->outFd, &le->line[from], to - from);
}


//
// Insert a character at the cursor location, move the cursor 
// forward and update the screen.
//

WARN_UNUSED static SwishResult LineAddChar(SwishLineEditor *le, char addCh)
{
    // Do we need to adjust the line buffer space?
    if (le->used >= le->space - 1)
    {
        // Make enough space for this extra character.
        size_t newLineLen = le->space * 2;
        char * newLine = SwishOsRealloc(le->line, newLineLen);
        if (newLine == NULL)
            return SWISH_ERROR_OUT_OF_MEMORY;

        le->line = newLine;
        le->space = newLineLen;
    }

    // Add it to the buffer.
    memmove(&le->line[le->cursorX+1], &le->line[le->cursorX], le->used - le->cursorX);
    le->line[le->cursorX] = addCh;
    le->used++;
    le->line[le->used] = 0;     // Null terminator.

    // Fix up the screen.
    SwishOsWrite(le->outFd, &le->line[le->cursorX], le->used - le->cursorX);
    le->cursorX++;
    DrawMoveBack(le, le->used - le->cursorX);

    return SWISH_OK;
}


//
// Delete a character at the cursor location and update the screen.
//

static void LineDeleteChar(SwishLineEditor *le)
{
    if (le->cursorX < le->used)
    {
        memmove(&le->line[le->cursorX], &le->line[le->cursorX + 1], le->used - le->cursorX + 1);
        le->used--;
        le->line[le->used] = 0;     // Null terminator.

        SwishOsWrite(le->outFd, &le->line[le->cursorX], le->used - le->cursorX);
        SwishOsPutc(le->outFd, ' ');
        DrawMoveBack(le, le->used + 1 - le->cursorX);
    }
}


//
// Replace a history line with new text.
//

WARN_UNUSED static SwishResult ReplaceHistory(SwishLineEditor *le, unsigned int historyLineNo, const char *line)
{
    if (le->history[historyLineNo] != NULL)
    {
        SwishOsFree(le->history[historyLineNo]);
    }

    le->history[historyLineNo] = SwishOsMalloc(strlen(line) + 1);
    if (le->history[historyLineNo] == NULL)
        return SWISH_ERROR_OUT_OF_MEMORY;

    strcpy(le->history[historyLineNo], line);
    
    return SWISH_OK;
}


//
// Add the most recent line to the history.
//

WARN_UNUSED static SwishResult AddToHistory(SwishLineEditor *le, const char *line)
{
    // Enter key. We're done. Add it to the history.
    if (le->historyLines == le->historyMaxLines)
    {
        // Free the oldest history line to make space.
        SwishOsFree(le->history[le->historyLines-1]);
    }
    else
    {
        // Add a new line for this new entry.
        le->historyLines++;
    }

    // Copy all the history entries up a slot.
    for (int i = le->historyLines - 1; i > 0; i--)
    {
        le->history[i] = le->history[i-1];
    }

    le->history[0] = NULL;

    // Replace the first history entry with our line.
    return ReplaceHistory(le, 0, line);
}


//
// Change from the current edit to one of the history lines.
//

WARN_UNUSED static SwishResult EditHistoryChangeLine(SwishLineEditor *le, int newHistoryLineNo)
{
    // Copy the current edit line to its respective history line.
    SwishResult rc = ReplaceHistory(le, le->historyCurrentLine, le->line);
    if (rc != SWISH_OK)
        return rc;

    // Erase the current edit.
    DrawMoveBack(le, le->cursorX);
    DrawRepeated(le, ' ', le->used);
    DrawMoveBack(le, le->used);
    le->used = 0;
    memset(le->line, 0, le->space);

    // Replace it with one from the history.
    // We know there's enough space in the edit line because it's as long or 
    // longer than the longest history line.
    le->historyCurrentLine = newHistoryLineNo;
    strcpy(le->line, le->history[le->historyCurrentLine]);
    le->used = strlen(le->line);
    le->cursorX = le->used;

    // Draw the new line and place the cursor at the end.
    SwishOsPuts(le->outFd, le->line);

    return SWISH_OK;
}


//
// Edit the previous history line.
//

WARN_UNUSED static SwishResult EditHistoryPrevious(SwishLineEditor *le)
{
    // Are we already at the earliest one?
    if (le->historyCurrentLine < le->historyLines-1)
        return EditHistoryChangeLine(le, le->historyCurrentLine + 1);

    return SWISH_OK;
}


//
// Edit the next history line.
//

WARN_UNUSED static SwishResult EditHistoryNext(SwishLineEditor *le)
{
    // Are we already at the most recent one?
    if (le->historyCurrentLine > 0)
        return EditHistoryChangeLine(le, le->historyCurrentLine - 1);

    return SWISH_OK;
}


//
// Get a line using line editing, history, etc.
//

SwishResult SwishLineEditorGetLine(SwishLineEditor *le, char **line)
{
    bool complete = false;

    // Make a new, blank history line at the end so we can start editing it.
    SwishResult rc = AddToHistory(le, "");
    if (rc != SWISH_OK)
        return rc;

    // Start the cursor at the start of the line.
    le->cursorX = 0;
    le->used = 0;
    le->historyCurrentLine = 0;
    memset(le->line, 0, le->space);

    do
    {
        // Get a key.
        int readCh;
        rc = SwishLineEditorGetKey(le, &readCh);
        if (rc != SWISH_OK)
            return rc;

        if (readCh < 0x20)
        {
            // Control characters.
            if (readCh == ENTER_KEY)
            {
                rc = ReplaceHistory(le, 0, le->line);
                if (rc != SWISH_OK)
                    return rc;

                SwishOsPuts(le->outFd, "\r\n");

                // The line we just edited is the most recent line in the history.
                *line = le->history[0];
                complete = true;
            }
        }
        else if (readCh < 0x7f)
        {
            // It's a printable character. Add it to the line.
            rc = LineAddChar(le, readCh);
            if (rc != SWISH_OK)
                return rc;
        }
        else if (readCh < 0x100)
        {
            if (readCh == BACKSPACE_KEY && le->cursorX > 0)
            {
                // Delete the character to the left of the cursor.
                le->cursorX--;
                DrawMoveBack(le, 1);
                LineDeleteChar(le);
            }
        }
        else
        {
            // It's an edit key.
            switch ((SwishKeyCode)readCh)
            {
                case SWISH_KEYCODE_LEFT_ARROW:
                    if (le->cursorX > 0)
                    {
                        // Move the cursor back.
                        DrawMoveBack(le, 1);
                        le->cursorX--;
                    }
                    break;

                case SWISH_KEYCODE_RIGHT_ARROW:
                    if (le->cursorX < le->used)
                    {
                        // Move the cursor forward.
                        DrawMoveForward(le, le->cursorX, le->cursorX+1);
                        le->cursorX++;
                    }
                    break;

                case SWISH_KEYCODE_HOME:
                    // Move to the start of the line.
                    DrawMoveBack(le, le->cursorX);
                    le->cursorX = 0;
                    break;

                case SWISH_KEYCODE_END:
                    // Move to the end of the line.
                    DrawMoveForward(le, le->cursorX, le->used);
                    le->cursorX = le->used;
                    break;

                case SWISH_KEYCODE_DELETE:
                    // Delete the character to the right of the cursor.
                    LineDeleteChar(le);
                    break;

                case SWISH_KEYCODE_UP_ARROW:
                    // Edit the previous history line.
                    rc = EditHistoryPrevious(le);
                    if (rc != SWISH_OK)
                        return rc;
                    break;

                case SWISH_KEYCODE_DOWN_ARROW:
                    // Edit the next history line.
                    rc = EditHistoryNext(le);
                    if (rc != SWISH_OK)
                        return rc;
                    break;

                default:
                    break;
            }
        }
    } while (!complete);
    
    return SWISH_OK;
}


//
// Gets a single key code, including ones which come as an escape sequence
// such as the arrow keys.
//

SwishResult SwishLineEditorGetKey(SwishLineEditor *le, int *key)
{
    char readCh;
    enum {
        KEY_STATE_IDLE,
        KEY_STATE_ESCAPED,
        KEY_STATE_ESCAPE_BRACKET,
        KEY_STATE_ABSORB_TILDE
    } state = KEY_STATE_IDLE;
    int keyCode = -1;
    int storedKeyCode = 0;

    do 
    {
        readCh = SwishOsGetc(le->inFd);
        if (readCh == -1)
            return SWISH_ERROR_IO;
        else
        {
            switch (state)
            {
                case KEY_STATE_IDLE:
                    if (readCh == ESCAPE_KEY)
                    {
                        state = KEY_STATE_ESCAPED;
                    }
                    else
                    {
                        keyCode = readCh;
                    }
                    break;

                case KEY_STATE_ESCAPED:
                    if (readCh == '[')
                    {
                        state = KEY_STATE_ESCAPE_BRACKET;
                    }
                    else
                    {
                        keyCode = readCh;
                    }
                    break;

                case KEY_STATE_ESCAPE_BRACKET:
                    switch (readCh)
                    {
                        case 'A':
                            keyCode = SWISH_KEYCODE_UP_ARROW;
                            break;

                        case 'B':
                            keyCode = SWISH_KEYCODE_DOWN_ARROW;
                            break;
                        
                        case 'C':
                            keyCode = SWISH_KEYCODE_RIGHT_ARROW;
                            break;
                        
                        case 'D':
                            keyCode = SWISH_KEYCODE_LEFT_ARROW;
                            break;
                        
                        case 'H':
                            keyCode = SWISH_KEYCODE_HOME;
                            break;
                        
                        case 'F':
                            keyCode = SWISH_KEYCODE_END;
                            break;
                        
                        case '2':
                            state = KEY_STATE_ABSORB_TILDE;
                            storedKeyCode = SWISH_KEYCODE_INSERT;
                            break;
                        
                        case '3':
                            state = KEY_STATE_ABSORB_TILDE;
                            storedKeyCode = SWISH_KEYCODE_DELETE;
                            break;
                        
                        case '5':
                            state = KEY_STATE_ABSORB_TILDE;
                            storedKeyCode = SWISH_KEYCODE_PAGE_UP;
                            break;
                        
                        case '6':
                            state = KEY_STATE_ABSORB_TILDE;
                            storedKeyCode = SWISH_KEYCODE_PAGE_DOWN;
                            break;
                    }
                    break;

                case KEY_STATE_ABSORB_TILDE:
                    if (readCh == '~')
                        keyCode = storedKeyCode;
                    else
                        keyCode = readCh;
                    break;
            }
        }
    } while (keyCode == -1);

    *key = keyCode;
    return SWISH_OK;
}
