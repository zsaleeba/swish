//
// A dictionary is a mapping from strings to something else.
// For instance a dictionary of strings maps from string keys to string 
// values.
//

#include <string.h>
#include <stdbool.h>

#include "dict.h"
#include "vector.h"
#include "os.h"


// Start with enough space for a few items.
#define START_DICT_SPACE 10


//
// Initialises a dict ready for use.
//

WARN_UNUSED SwishResult SwishDictInit(SwishDict *d, SwishFreeValueFunc free_value)
{
    d->free_value = free_value;
    return SwishVectorInit(&d->v, sizeof(SwishDictKv), START_DICT_SPACE);
}


//
// Frees any storage used by the dict.
//

void SwishDictDeinit(SwishDict *d)
{
    // Free all the keys and values.
    for (unsigned int i = 0; i < d->v.items; i++)
    {
        SwishDictKv *kv = &VEC(&d->v, i, SwishDictKv);
        SwishOsFree(kv->key);

        if (kv->value != NULL)
        {
            d->free_value(kv->value);
        }
    }

    // Free the vector.
    SwishVectorDeinit(&d->v);
}


//
// Performs a binary search on the sorted keys to find an item,
// or find where it can be inserted.
// Returns true if found and sets index to the location it was
// found at, or if not found the location to insert it at.
//

WARN_UNUSED static bool FindIndex(SwishDict *d, const char *key, unsigned int *index)
{
    unsigned int from = 0;
    unsigned int to = d->v.items;
    unsigned int middle = 0;
    int cmp;

    while (from != to)
    {
        // We're searching the interval [from..to) since to is beyond the end
        // of the vector.
        middle = (to - from) / 2 + from;
        cmp = strcmp(key, VEC(&d->v, middle, SwishDictKv).key);
        if (cmp < 0)
        {
            // It's before the middle.
            to = middle;
        }
        else if (cmp > 0)
        {
            // It's after the middle.
            from = middle + 1;
        }
        else
        {
            // Exact match.
            *index = middle;
            return true;
        }
    }

    // We didn't find it but we now know where to insert it.
    *index = middle;
    return false;
}


//
// Add a new item to the dictionary.
//

WARN_UNUSED SwishResult SwishDictAdd(SwishDict *d, const char *key, void *value)
{
    SwishResult rc = SWISH_OK;
    unsigned int index;

    if (FindIndex(d, key, &index))
    {
        // It already exists in the dict - replace it.
        void *old_value = VEC(&d->v, index, SwishDictKv).value;
        if (old_value != NULL)
        {
            d->free_value(old_value);
        }

        VEC(&d->v, index, SwishDictKv).value = value;
    }
    else
    {
        // Insert it in order.
        rc = SwishVectorAdd(&d->v, index, value);
    }

    return rc;
}


//
// Removes an item from the dict.
// Returns true if it was found and removed. false if not present.
//

bool SwishDictDelete(SwishDict *d, const char *key)
{
    unsigned int index;

    if (FindIndex(d, key, &index))
    {
        // Found - remove it.
        SwishVectorDelete(&d->v, index);
        return true;
    }
    else
    {
        return false;
    }
}


//
// Finds an item in the dict and returns it.
// Returns NULL if not found.
//

WARN_UNUSED void *SwishDictFind(SwishDict *d, const char *key)
{
    unsigned int index;

    if (FindIndex(d, key, &index))
    {
        return VEC(&d->v, index, SwishDictKv).value;
    }
    else
    {
        return NULL;
    }
}
