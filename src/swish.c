#include <unistd.h>
#include <string.h>
#include <stdio.h>

#include "swish.h"
#include "lineeditor.h"
#include "os.h"


#define PROMPT "$ "


//
// Initialise the swish shell. This dynamically allocates storage so 
// you must call SwishShellDeinit() when you're done.
//

WARN_UNUSED SwishResult SwishShellInit(SwishShell *ss, int inFd, int outFd, int historyMaxLines)
{
    memset(ss, 0, sizeof(*ss));
    ss->inFd = inFd;
    ss->outFd = outFd;
    ss->historyMaxLines = historyMaxLines;

    return SWISH_OK;
}


//
// Frees all resources associated with the shell.
//

void SwishShellDeinit(SwishShell *ss)
{
    SwishLineEditorDeinit(&ss->lineEditor);
}


WARN_UNUSED SwishResult SwishShellAddCommand(SwishShell *ss, SwishCommandDef *def)
{
    (void)ss;
    (void)def;
    return SWISH_OK;
}


//
// Execute a single line command.
//

WARN_UNUSED static SwishResult ExecuteCommand(SwishShell *ss, const char *cmd)
{
    (void)ss;
    (void)cmd;
    return SWISH_OK;
}


//
// Run the command line. 
// Only returns if a command calls SwishShellCommandLineExit().
//

WARN_UNUSED SwishResult SwishShellCommandLine(SwishShell *ss)
{
    SwishResult rc = SWISH_OK;
    char *line = NULL;
    
    rc = SwishLineEditorInit(&ss->lineEditor, ss->inFd, ss->outFd, ss->historyMaxLines);
    if (rc != SWISH_OK)
        return rc;

    do
    {
        SwishOsPuts(ss->outFd, PROMPT);
        rc = SwishLineEditorGetLine(&ss->lineEditor, &line);

        if (rc == SWISH_OK)
        {
            rc = ExecuteCommand(ss, line);
        }
    } while (rc == SWISH_OK && strcmp(line, "quit") != 0);
    

    SwishLineEditorDeinit(&ss->lineEditor);

    return rc;
}


//
// Return an error string corresponding to the error code.
//

WARN_UNUSED const char *SwishErrorStr(SwishResult r)
{
    switch (r)
    {
        case SWISH_OK:
            return "ok";
        
        case SWISH_ERROR_OUT_OF_MEMORY:
            return "out of memory";

        case SWISH_ERROR_IO:
            return "io error";
    }

    return "-";
}
