#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "os.h"


void *SwishOsMalloc(size_t size)
{
    return malloc(size);
}


void *SwishOsRealloc(void *mem, size_t size)
{
    return realloc(mem, size);
}


void SwishOsFree(void *mem)
{
    free(mem);
}


char SwishOsGetc(int fd)
{
    char readCh;
    int rc = read(fd, &readCh, 1);
    if (rc == -1)
        return -1;
    
    return readCh;
}

void SwishOsPutc(int fd, char ch)
{
    write(fd, &ch, 1);
}

void SwishOsWrite(int fd, const char *buf, size_t len)
{
    write(fd, buf, len);
}

void SwishOsPuts(int fd, const char *buf)
{
    SwishOsWrite(fd, buf, strlen(buf));
}
