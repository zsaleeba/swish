#include <string.h>

#include "common.h"
#include "vector.h"
#include "os.h"


//
// Initialise the vector.
//

WARN_UNUSED SwishResult SwishVectorInit(SwishVector *v, size_t item_size, size_t start_space)
{
    memset(v, 0, sizeof(*v));
    v->vec = SwishOsMalloc(item_size * start_space);
    if (v->vec == NULL)
        return SWISH_ERROR_OUT_OF_MEMORY;

    v->item_size = item_size;
    v->items = 0;
    v->space = start_space;

    return SWISH_OK;
}


//
// Free storage used by the vector. Won't free any pointers stored in the
// vector - only the vector's storage.
//

void SwishVectorDeinit(SwishVector *v)
{
    if (v->vec != NULL)
    {
        SwishOsFree(v->vec);
    }

    v->items = 0;
    v->space = 0;
}


//
// Add an item to the vector at the given index, moving all the following
// items up one slot.
//

WARN_UNUSED SwishResult SwishVectorAdd(SwishVector *v, unsigned int index, void *new_item_ptr)
{
    // Check index for validity.
    if (index > v->items)
        return SWISH_OK;

    if (v->items >= v->space - 1)
    {
        // Make more space in the vector.
        size_t new_space = v->space * 2;
        void *new_vec = SwishOsRealloc(v->vec, v->item_size * new_space);
        if (new_vec == NULL)
            return SWISH_ERROR_OUT_OF_MEMORY;
        
        v->vec = new_vec;
        v->space = new_space;
    }

    // Move the following items up one slot.
    memmove(v->vec + (index + 1) * v->item_size, v->vec + index, (v->items - index) * v->item_size);
    v->items++;

    // Add our item.
    memcpy(v->vec + index * v->item_size, new_item_ptr, v->item_size);

    return SWISH_OK;
}


//
// Remove an item from the vector. Note that this only removes the
// item from the vector - it doesn't free any storage referred to
// by the item.
//

void SwishVectorDelete(SwishVector *v, unsigned int index)
{
    // Check index for validity.
    if (index >= v->items)
        return;

    // Move everything down a slot.
    memmove(v->vec + index * v->item_size, v->vec + (index + 1) * v->item_size, (v->items - index - 1) * v->item_size);
    v->items--;
}
