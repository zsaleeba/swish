#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>
#include <termios.h>
#include <memory.h>

#include "swish.h"


// The number of lines of history to keep.
#define MAX_HISTORY_LINES 10



//
// Save the terminal modes so we can restore them at the end.
//

void SaveTerminalModes(struct termios *stdinMode, struct termios *stdoutMode)
{
    if (tcgetattr(0, stdinMode) == -1)
    {
        fprintf(stderr, "can't get stdin terminal modes: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    if (tcgetattr(1, stdoutMode) == -1)
    {
        fprintf(stderr, "can't get stdin terminal modes: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}


//
// Restore the original terminal modes.
//

void RestoreTerminalModes(struct termios *stdinMode, struct termios *stdoutMode)
{
    tcsetattr(0, TCSAFLUSH, stdinMode);
    tcsetattr(1, TCSAFLUSH, stdoutMode);
}

//
// Set raw input and output.
//

void SetRawIo(struct termios *stdinMode, struct termios *stdoutMode)
{
    struct termios inMode;
    struct termios outMode;

    memcpy(&inMode, stdinMode, sizeof(struct termios));
    cfmakeraw(&inMode);
    if (tcsetattr(0, TCSAFLUSH, &inMode) == -1)
    {
        RestoreTerminalModes(stdinMode, stdoutMode);
        fprintf(stderr, "can't set stdin terminal modes: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }

    memcpy(&outMode, stdoutMode, sizeof(struct termios));
    cfmakeraw(&outMode);
    if (tcsetattr(1, TCSAFLUSH, &outMode) == -1)
    {
        RestoreTerminalModes(stdinMode, stdoutMode);
        fprintf(stderr, "can't set stdout terminal modes: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
    }
}


//
// main program.
//

int main()
{
    // Terminal modes are saved here.
    struct termios stdinMode;
    struct termios stdoutMode;
    SwishShell     shell;
    SwishResult    rc = SWISH_OK;

    // Set terminal modes to raw.
    SaveTerminalModes(&stdinMode, &stdoutMode);

    SetRawIo(&stdinMode, &stdoutMode);

    // Run the shell.
    rc = SwishShellInit(&shell, 0, 1, MAX_HISTORY_LINES);
    if (rc == SWISH_OK)
    {
        rc = SwishShellCommandLine(&shell);
        SwishShellDeinit(&shell);
    }

    // Fix the terminal modes.
    RestoreTerminalModes(&stdinMode, &stdoutMode);

    // Report any errors.
    if (rc != SWISH_OK)
    {
        printf("shell error: %s\n", SwishErrorStr(rc));
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
