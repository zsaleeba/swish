//
// A StrDict is a dictionary of strings. It's useful for environment and
// symbol tables.
//
// The StrDict is implemented here using Dict, which is in turn implemented
// as a sorted vector of key/value pairs.
//

#include <stdbool.h>
#include <string.h>

#include "strdict.h"
#include "dict.h"
#include "os.h"


//
// Initialises a StrDict ready for use.
//

WARN_UNUSED SwishResult SwishStrDictInit(SwishStrDict *sd)
{
    return SwishDictInit(sd, &SwishOsFree);
}


//
// Frees any storage used by the StrDict.
//

void SwishStrDictDeinit(SwishStrDict *sd)
{
    SwishDictDeinit(sd);
}


//
// Add a new item to the StrDict.
//

WARN_UNUSED SwishResult SwishStrDictAdd(SwishStrDict *sd, const char *key, const char *value)
{
    void *val_copy = SwishOsMalloc(strlen(value) + 1);
    strcpy(val_copy, value);
    return SwishDictAdd(sd, key, val_copy);
}


//
// Removes an item from the StrDict.
// Returns true if it was found and removed. false if not present.
//

bool SwishStrDictDelete(SwishStrDict *sd, const char *key)
{
    return SwishDictDelete(sd, key);
}


//
// Finds an item in the StrDict and returns it.
// Returns NULL if not found.
//

WARN_UNUSED const char *SwishStrDictFind(SwishStrDict *sd, const char *key)
{
    return SwishStrDictFind(sd, key);
}
